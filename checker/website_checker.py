""" Module to check the connection to a website
"""

import pycurl
try:
    from io import BytesIO
except ImportError:
    from StringIO import StringIO as BytesIO

class WebsiteChecker:
    """ Class to check the connection to a website
        The website is insiden a object of the class WebsiteInput that contains its name and checkinterval
    """

    def __init__(self, websiteInput):
        self.websiteInput = websiteInput

    def testConnection(self):
        """ Method to test a connection to the website given as parameter to the object constructor
            It returns a dictionary with the following keys:
                time: total time to obtain the response of the site to a GET method
                code: code returned by the side to a GET method
        """

        # For future updates, if we want to get another metrics from the request's result, we can add them in the returned dictionary :)

        # As we've specified a checkinterval, we have to assure that the request don't surpass this time.
        # Therefore we use a timetout with checkinterval value. Since the WebsiteUpdate class uses the results of this method
        # to push them in the respective windows before doing the next request, we specify a timeout slightly smaller than checkInterval
        try:

            buffer = BytesIO()
            c = pycurl.Curl()
            c.setopt(c.URL, self.websiteInput.name)
            c.setopt(c.FOLLOWLOCATION, True) # To request the redirected link if the site's been redirected
            c.setopt(c.TIMEOUT_MS, self.websiteInput.checkInterval - 500)
            c.setopt(c.NOSIGNAL,1) # According to libcurl, in a multithread program , NOSIGNAL must be set to 1
            c.setopt(c.WRITEDATA, buffer)
            c.perform()

            result = {}
            result["time"] = c.getinfo(c.TOTAL_TIME)
            result["code"] = c.getinfo(c.RESPONSE_CODE)
            c.close()

            return result

        # If the timeout was reached, we return a -1 to indicate an invalid time and the code "TIMEOUT"
        except Exception as e:
            result = {}
            result["time"] = -1
            result["code"] = "TIMEOUT"
            return result
