""" This module provides a class to act as a buffer to store information to be
    printde on the screen
"""

from threading import Lock

class OutputBuffer:
    """ Class to store a list of string to be shown on the screen
        As many thread can use one instance at once, it has a lock to handle race conditions
    """
    def __init__(self):
        self.outputString = [] # List of strings to be stored
        self.lock = Lock() # Lock to handle race condition

    def append(self, newString):
        """ Methos to append a new string to the buffer
        """
        with self.lock:
            self.outputString.append(newString)

    def clear(self):
        """ Method to clear the buffer, deleting all stored strigs
        """
        with self.lock:
            self.outputString = []

    def getBuffer(self):
        """ Methotd to return a list of all strings stored in the buffer 
        """
        with self.lock:
            return self.outputString
