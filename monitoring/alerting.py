""" This module offers the alerting classes that can observe windows objects and trigger alerting to the user
"""

from .observer_pattern import Observer
import datetime

class AvailabilityAlert(Observer):
    """ A class that implements the Observer abstract class. It observer a floatwindow and triggers an alerting when the availability is under 80%
    """

    def __init__(self, floatWindow, streamBuffer, alertingBuffer):
        """ We must provide the floatWindow to be oberseved as a constructor parameter
        """
        super().__init__(floatWindow)
        self.__alerted = False # Informs if the class has already alerted the user. Used to avoid repeted alert messages on the screen
        self.streamBuffer = streamBuffer
        self.alertingBuffer = alertingBuffer

    def update(self, floatWindow):
        """ Overrides the update method of the Overver abstract class
            Each time a new element is inserted in the screen, if the new availability is under 0.8, shows a message on the screen
        """

        # We can show an alerting message only if a window is already full
        if not floatWindow.isWindowFull:
            # print ("not enough information")
            return

        # If the availability is under 80% and we haven't already alerted the user
        if floatWindow.availability < 0.8 and not self.__alerted:
            outputString = "Website {0} is down. Availability: {1:0.2f}%, time:{2}".format(floatWindow.website.name, floatWindow.availability*100, str(datetime.datetime.now()))

            # The alerting messages must be printed on both buffers
            self.streamBuffer.append(outputString)
            self.alertingBuffer.append(outputString)
            # print("Website {0} is down. Availability: {1:0.2f}%, time:{2}".format(floatWindow.website.name, floatWindow.availability*100, str(datetime.datetime.now())) )
            self.__alerted = True

        # If the site recovers after alerting the user
        elif floatWindow.availability >= 0.8 and self.__alerted:
            outputString = "Website {} recovered".format(floatWindow.website.name)

            # The alerting messages must be printed on both buffers
            self.streamBuffer.append(outputString)
            self.alertingBuffer.append(outputString)
            # print("Website {} recovered".format(floatWindow.website.name))
            self.__alerted = False
