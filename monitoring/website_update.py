""" This module constains a class to update the windows related to a website each 'checkInterval'
    period of time
"""

from threading import Thread
from checker import WebsiteChecker

import time

class WebsiteUpdate(Thread):
    """ This class asks the websiteChecker each websiteInput.checkInterval period of time to check the website.
        It updates the timeWindows and codeWindows that are passed as lists to the object constructor
        This class inherits from Thread since we want to check the site and update the corresponding windows every checkInterval miliseconds
    """

    def __init__(self, websiteInput, websiteChecker, timeWindowsList, codeWindowsList):
        """ We must pass a WebsiteInput object with a site name and check interval, a websiteChecker already
            configured to requests this site, a list of timeWindows (FloatWindow) and a list of codeWindows (StringWindow)
            to be updated with the site response
        """
        Thread.__init__(self)
        self.websiteInput = websiteInput
        self.websiteChecker = websiteChecker
        self.timeWindowsList = timeWindowsList
        self.codeWindowsList = codeWindowsList
        self.working = True # Attribute to control when the thread should stop to work

    def run(self):

        # The thread works until its attribute working is set to False
        while (self.working):

            # The thread must check the website and update the windows during, at the worst case, the checkInterval
            startTime = time.time()*1000

            # Check the website
            checkResult = self.websiteChecker.testConnection()
            checkTime = checkResult["time"]
            checkCode = checkResult["code"]

            # Updates each window
            for timeWindow in self.timeWindowsList:
                timeWindow.push(checkTime)

            for codeWindow in self.codeWindowsList:
                codeWindow.push(checkCode)

            # If the checkInterval slot hasn't been already wasted, the thread sleep until make a new update
            endTime = time.time()*1000
            elapsedTime = endTime - startTime
            if elapsedTime < self.websiteInput.checkInterval:
                time.sleep((self.websiteInput.checkInterval - elapsedTime)/1000)
