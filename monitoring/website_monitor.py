""" This class wraps all the logic to monitor a website, creating all the necessary objects to hold the data
    and update them. It alsos create the threads to monitor a website and controls their execution
"""

from threading import Thread
from checker import WebsiteChecker
from monitoring import FloatWindow, StringWindow, AvailabilityAlert
from .website_update import WebsiteUpdate
from .print_stats import PrintStats

import time

class WebsiteMonitor():
    """ This class wraps the logic to monitor a website

    """

    def __init__(self, websiteInput, streamBuffer, alertingBuffer ):
        """ We must pass the WebsiteInput object representing a website to the class constructor
            We also provide a streamBuffer and alertingBuffer (instances of OutputBuffer class) that
            are used by the PrintStats and AvailabilityAlert objects to print their messages
        """
        self.websiteInput = websiteInput
        self.streamBuffer = streamBuffer
        self.alertingBuffer = alertingBuffer

        # Variables to hold the threads that this class uses to monitor the website
        self.websiteUpdate = None
        self.printStats_10s = None
        self.printStats_1min = None

    def start(self):
        """ Method to start the monitorig of the website
        """

        # Create the desired windows to monitor the website
        # For consistency reasons, we use time in milliseconds throughout the project
        # The time windows hold the information about the website's response time
        # The code windows hold the information about the codes issued by the site as response the to get method

        # 2 minutes -> for the availability alert
        timeFrame_2_minutes = 2*60*1000
        timeWindow_2_minutes = FloatWindow(timeFrame_2_minutes, self.websiteInput)

        # 10 minutes
        timeFrame_10_minutes = 10*60*1000
        timeWindow_10_minutes = FloatWindow(timeFrame_10_minutes, self.websiteInput)
        codeWindow_10_minutes = StringWindow (timeFrame_10_minutes, self.websiteInput)

        # 1 hour
        timeFrame_1_hour = 1*60*60*1000
        timeWindow_1_hour = FloatWindow(timeFrame_1_hour, self.websiteInput)
        codeWindow_1_hour = StringWindow (timeFrame_1_hour, self.websiteInput)

        # Create a website checker to check the desired website
        websiteChecker = WebsiteChecker(self.websiteInput)

        # Create a websiteUpdate thread to update the corresponding windows
        self.websiteUpdate = WebsiteUpdate(self.websiteInput, websiteChecker, [timeWindow_2_minutes, timeWindow_10_minutes,  timeWindow_1_hour],[codeWindow_10_minutes, codeWindow_1_hour])

        # Create the availability observer to alert the user if the availability of a site is < 80%. We use a time windows of 2min
        availabilityAlert = AvailabilityAlert(timeWindow_2_minutes, self.streamBuffer, self.alertingBuffer)

        # Create the threads to print the stats from the website
        self.printStats_10s = PrintStats(self.websiteInput.name, 10 * 1000, timeWindow_10_minutes, codeWindow_10_minutes, self.streamBuffer)
        self.printStats_1min = PrintStats(self.websiteInput.name, 1 * 60 * 1000, timeWindow_1_hour, codeWindow_1_hour, self.streamBuffer)

        # Starts all the necessary threads to monitor the website
        self.websiteUpdate.start()
        self.printStats_10s.start()
        self.printStats_1min.start()

    def stop(self):
        """ Method to stop the monitoring of the website
        """

        # By changing the working attribute of the objects from the classes WebsiteUpdate and PrintStats, we stop their execution (they'll finish the run method)
        self.websiteUpdate.working = False
        self.printStats_10s.working = False
        self.printStats_1min.working = False

    def waitThreadsToFinish(self):
        """ Method to wait all threads launched by the monitor finish
        """
        self.websiteUpdate.join()
        self.printStats_10s.join()
        self.printStats_1min.join()
