""" This module implements the Observer Design Pattern
"""

from abc import ABCMeta, abstractmethod

class Observable:
    """ The observable class has a list of observers that are notified since there are mofidications on the observable that they must be aware
    """
    def __init__(self):
        self.observers = []

    def register_observer(self, newObserver):
        """ Method to register an observer to this observable
        """
        self.observers.append(newObserver)

    def notify(self):
        """ Method to notify all the registered observers of modifications in the obersable.
            The observers get a reference to the observable and thus can access their public attributes
        """
        for observer in self.observers:
            observer.update(self)

class Observer(metaclass=ABCMeta):
    """ This class can subscribe to an observable to get notified every time that the latter calls the notify method
        It's an abstract class since each subclass must override the update method to decide what to do when they're notified
    """
    def __init__(self, observable):
        """ We must provide the observable as a parameter to the observer
        """
        observable.register_observer(self)

    @abstractmethod
    def update(self, observable):
        pass
