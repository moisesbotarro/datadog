""" This module contains the class to print the stats from a floatWindow and a StringWindow
"""

from threading import Thread
import time

class PrintStats(Thread):
    """ This class reads a FloatWindow and a CodeWindow to print its stats each <printInterval> milliseconds
        Since we want to print the stats each <printInterval> milliseconds, this class inherits from Thread to run
        independently
    """

    def __init__(self, websiteName, printInterval, timeWindow, codeWindow, streamBuffer):
        """ We must pass the website name, the derided printInteval, the floatWindow and the stringWindow to be analyzed
        """
        Thread.__init__(self)
        self.websiteName = websiteName
        self.printInterval = printInterval
        self.timeWindow = timeWindow
        self.codeWindow = codeWindow
        self.working = True # Attribute to control when the thread should stop to work
        self.streamBuffer = streamBuffer

    def run(self):

        # At the first run, we must wait printInterval milliseconds to print the first stats
        time.sleep(self.printInterval/1000)

        # The thread works until its attribute working is set to False
        while (self.working):

            # Print stats on the streamBuffer 
            codesStats = self.codeWindow.getStringsStat()
            outputString = "{0: <35} <last {1:0.0f}min> availability:{2:6.2f}% max:{3:4.2f}s min:{4:4.2f}s avg:{5:4.2f}s codesCount:[{6}]".format(self.websiteName, self.timeWindow.timeFrame/(60*1000), self.timeWindow.availability*100, self.timeWindow.max, self.timeWindow.min, self.timeWindow.average, codesStats)
            self.streamBuffer.append( outputString )
            # print("{0: <35} <last {1:0.0f}min> availability:{2:6.2f}% max:{3:4.2f}s min:{4:4.2f}s avg:{5:4.2f}s codesCount:[{6}]\n".format(self.websiteName, self.timeWindow.timeFrame/(60*1000), self.timeWindow.availability*100, self.timeWindow.max, self.timeWindow.min, self.timeWindow.average, codesStats), end="")
            # print (" codesCount:{ " + self.codeWindow.getStringsStat() + "}")

            # Wait <printInterval> milliseconds to print the stats again
            time.sleep(self.printInterval/1000)
