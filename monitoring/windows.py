""" Module that contains window classes to store elements
"""

from collections import deque
from abc import ABCMeta, abstractmethod
from .observer_pattern import Observable

import time
import datetime

# We can consider a stream of data to be analyzed. A window will move through this stream of data. The length of the window is given by its
# timeFrame property. The push method inserts a new element in the window and it can be seen as moving the window to the right. The class also
# allos the computation of some statistics with the elements inside the window
class Window(metaclass=ABCMeta):
    """ An abstract class that represents a window to store some data during a timeFrame amount of time.
        It's possible to compute some statistics with the data inside the window
        As it can store any type of data, this class must be extended to deal with specific types of data (numbers, strings, etc...)
    """

    def __init__(self, timeFrame, website):
        "A window has to be initialized with its duration in ms (timeFrame) and the website(as a WebsiteInput object) whose informations it stores"
        self.window = deque() # The move window with duration given by timeFrame to store the elements
        self.timeFrame = timeFrame # Length in time of the window (in miliseconds)
        self.startTime = None # Store the time from which we start to measure the timeFrame

        # When the window reachs its length given by timeFrame, isWindowFull becomes True and every insertion of a new element
        # if followed by the deletion of the older element of the window (left-most element of the deque)
        self.isWindowFull = False
        self.website = website # WebsiteInput object that contains website name and its check interval


    def printWindow(self):
        """ Print the deque that represents the window with the inserted elements """
        print(self.window)

    @abstractmethod
    def updateStats(self, deletedElement, newElement):
        """ Method to compute some statistics with the elements inside a window
        """
        return


    def __replaceElement(self, newElement):
        """ Function to pop the left-most element of the window and insert the new one on its right.
            Returns the poped element
        """
        self.window.append(newElement)
        return self.window.popleft()

    def push(self, newElement):
        """ Method to insert a new element in the window.
            It handles the case where the elapsed time since the insertion of the first element > timeFrame and hence the window has to move, removing
            the left-most element and inserting the new one on the right
        """

        # If it's the first time where the window is being used, we have to save the time to determine when we start to pop elements from the
        # left of the window
        if (self.startTime == None):
            self.startTime = time.time() * 1000;


        deletedElement = None
        # If the window is already full, we must pop its first element before inserting a new one
        if self.isWindowFull:
            # print("Moving Window")
            deletedElement =  self.__replaceElement(newElement)

        # Otherwise, we insert a new element in the window.
        else:

            # The time until the first insertion on the window
            elapsedTime = (time.time() * 1000) - self.startTime

            # If at this time the window has got full
            if elapsedTime > self.timeFrame:
                # print("Windows has just got full")
                self.isWindowFull = True
                deletedElement = self.__replaceElement(newElement)

            # The window is not yer full (length of the window < timeFrame)
            else:
                # print("Filling window")
                self.window.append(newElement)

        # Afther the insertion of the new element, we update the statistics associated with the window
        self.updateStats(deletedElement, newElement)


class StringWindow(Window):
    """ A window that stores string objects
        As statistics it offers the different instances of string that it has with their associated count.
    """

    def __init__(self, timeFrame, website):
        super().__init__(timeFrame,website)
        self.__stringsFrequency = {}
        self.website = website

    def updateStats(self, deletedElement, newElement):
        """ Method to count the different instances of string inside the window
        """

        # Increase the count of the added string
        if newElement not in self.__stringsFrequency:
            self.__stringsFrequency[newElement] = 1

        else:
            self.__stringsFrequency[newElement] += 1

        # Decrease the count of the removed string
        if deletedElement != None:
            self.__stringsFrequency[deletedElement] -= 1

            # If the removed string is not more present in the window, we can erase the associated key in the dictionary of frequencies
            if self.__stringsFrequency[deletedElement] == 0:
                del self.__stringsFrequency[deletedElement]

    def getStringsStat(self):
        """ Get a string representing the different strings present in the windows and their corresponding counts
        """

        stringsStat = ""

        for tag, freq in self.__stringsFrequency.items():
            stringsStat += "\"" + str(tag) + "\":" + str(freq) + " "

        return stringsStat

class FloatWindow(Window, Observable):
    """ A window that stores float objects
        As statistics it offers their max, min and average. Since this class stores the response time of a site, it also offers the availability of
        the website
        These stats can be accessed by the public attributes: min, max, average and availability
        This class also inherit from an Observable which means that it implements the Observer Design Pattern
        It notifies its observers when a modification is done inside the window in order to let them alert some  behaviours
    """

    # We consider a valid element a response time that hasn't triggered a timeout while checking a website
    # If a website doesn't respond within the timeout, the websiteChecker returns a -1. -1 are called here 'timeoutElements'. It'll be usefull
    # to compute the availability of the website
    def __init__(self, timeFrame, website):
        Window.__init__(self, timeFrame=timeFrame, website=website)
        Observable.__init__(self)
        self.average = float("inf") # # Start value for the average
        self.__averageNumerator = 0 # Used to calculate faster the average of the window
        self.max = -float("inf") # Start value for the max
        self.min = float("inf") # Start value for the min
        self.__validElements = 0
        self.__timeoutElements = 0
        self.availability = 1

    def __computeAverage(self, deletedElement, newElement):
        """ Compute the average of the float elements inside the window
        """

        # We sum the just pushed element in the average numerator and subtract the deleted one if they are valid times
        if newElement != -1:
            self.__averageNumerator += newElement

        if deletedElement != -1:
            self.__averageNumerator -= deletedElement

        # The timeout elements don't participate in the computation of the average
        if self.__validElements != 0:
            self.average = self.__averageNumerator / self.__validElements

        # If there isn't valid elements in the window, we put +inf as the average
        else:
            self.average = float("inf") # Reset the average

    def __computeMax(self, deletedElement, newElement):
        """ Compute the biggest float inside the window
        """

        # Timeout elements don't participate in the computation of the max
        # If the newElement is bigger than the current max, we can just update the current max
        if newElement != -1 and newElement > self.max:
            self.max = newElement
            return

        # If the deleted element was the current max, we have to iterate through all over the window to find the max float amongst the valid ones
        if deletedElement == self.max:
            if self.__validElements == 0:
                self.max = -float("inf") # Reset the max

            else:
                self.max = max([time for time in self.window if time != -1])

    def __computeMin(self, deletedElement, newElement):
        """ Compute the smallest float inside the window
        """

        # Timeout elements don't participate in the computation of the min
        # If the newElement is smaller than the current min, we can just update the current min
        if newElement != -1 and newElement < self.min:
            self.min = newElement
            return

        # If the deleted element was the current min, we have to iterate through all over the window to find the min float amongst the valid ones
        if deletedElement == self.min:
            if self.__validElements == 0:
                self.min = float("inf") # Reset the min

            else:
                self.min = min([time for time in self.window if time != -1])

    def __computeAvailability(self):
        """ Compute the availability of the site whose reponse time we put in the current window
        """

        # To compute the availability, we just have to compute the ratio of the valid elements over the total elements on the window
        self.availability = self.__validElements / (self.__validElements + self.__timeoutElements)


    def updateStats(self, deletedElement, newElement):
        """ Method to update the stats of the floats inside the window
        """

        # We increase the amount of valid or time outs elements according to the type of the new element
        if newElement == -1:
            self.__timeoutElements += 1

        else:
            self.__validElements += 1


        # If there's an element that was pulled from the window...
        if deletedElement != None:

            # ...we decreased the corresponding element time
            if deletedElement == -1:
                self.__timeoutElements -= 1

            else:
                self.__validElements -= 1

            # ... we update the stats with the deleted and inserted element
            self.__computeAverage(deletedElement, newElement)
            self.__computeMax(deletedElement, newElement)
            self.__computeMin(deletedElement, newElement)


        # Otherwise, we pass neutral elements as a deleted element to compute the stats
        else:
            self.__computeAverage(0, newElement)
            self.__computeMax(-float("inf"), newElement)
            self.__computeMin(float("inf"), newElement)

        self.__computeAvailability()

        # If there are observers that've subscribed to observer the window, we inform them about the changes
        self.notify()
