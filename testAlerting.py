""" This file implements unitTests to test the Alerting Logic
    To run it do:
    python testAlerting.py
"""


import unittest
import time
import io, sys

from input import WebsiteInput
from monitoring import AvailabilityAlert, FloatWindow, OutputBuffer

# Function to push a synthtetic time response in a FloatWindow using a checkInterval time
def checkWebsite(floatWindow, timeResponse, checkInterval):
    checkInterl_s = checkInterval/1000  #time.time() gives a time and time.sleep() receives a time in seconds
    start = time.time()
    floatWindow.push(timeResponse)
    end = time.time()
    elapsed = end - start
    if elapsed < checkInterl_s:
        time.sleep(checkInterl_s - elapsed)


class TestAvailabilityAlerting(unittest.TestCase):

    # Test a scenario where after the first 2 minuts the site has an availability of 0.85. Then it goes down below 0.8, triggering an alert.
    # It stays below 0.8 and thus we verify that no more alert was trigeered.
    # To finish, we increase the availibity to 0.8 to check if the recovered message is showed. We increase it a little more to verify
    # that the last message is showed only once
    def test_scenario_1(self):

        # Bufers where the Availability alert will print the alerting message
        globalStream = OutputBuffer()
        alertingStream = OutputBuffer()

        # We don't want to rely in a internet connection to test our class. Hence, we'll
        # create synthetic response each 2s and put it in the FloatWindow window
        # As the window has 2min length, a full window has 60 elements
        # We call an invalid element an element that represents that the server hasn't responded (-1)
        testWebsiteInput = WebsiteInput("site-test", 2000)
        floatWindow_2min = FloatWindow(2*60*1000, testWebsiteInput)
        availabilityAlert = AvailabilityAlert(floatWindow_2min, globalStream, alertingStream)

        #During the first 2min, we simulate an availabilty of 0.85

        for i in range(0,51):
            checkWebsite(floatWindow_2min, 0.1, testWebsiteInput.checkInterval)

        for i in range(0,9):
            checkWebsite(floatWindow_2min, -1, testWebsiteInput.checkInterval)


        # At this time, the availability must be equal to 0,85 and thus no alerting should have been printed. As 2 min has been passed,
        # each new element inserted will causa a left-element in the windows to be removed

        # We want to pass the threshold of 0.8. By inserting 4 timeout in the window, we'll have an availability of 47/60 = 0.78333


        for i in range(0,4):
            checkWebsite(floatWindow_2min, -1, testWebsiteInput.checkInterval)

        # We expect only one output with the alert for the website
        self.assertEqual(len(alertingStream.getBuffer()), 1)
        self.assertIn("Website site-test is down. Availability: 78.33%", alertingStream.getBuffer()[0])

        # We'll insert more timeouts in the next 10s but no alerting must been triggered since one has been alredy been printed
        # The window will have (47-5)/60 = 0.7 of availability
        for i in range(0,5):
            checkWebsite(floatWindow_2min, -1, testWebsiteInput.checkInterval)

        # We expect that no more alerting message has been shown
        self.assertEqual(len(alertingStream.getBuffer()), 1)
        self.assertIn("Website site-test is down. Availability: 78.33%", alertingStream.getBuffer()[0])

        # Now, we want that the site recovers from timeout. The desired availabilty is 0.8 = 48.
        # Until now we have 42 valid elements but in the window's first positions. We have 18 invalid elements in the last positions
        # At each insertion, one valid element is also removed.
        # Hence, we have to remove 6 invalid elements to get the availibity of 0.8 and before that we have to remove all the first
        # 42 valid ones replacing them by other 42 valid elements
        for i in range(0,48):
            checkWebsite(floatWindow_2min, 0.1, testWebsiteInput.checkInterval)

        # We expect that until now, we have 2 messages. One saying that the site is down and another for its recovery
        self.assertEqual(len(alertingStream.getBuffer()), 2)
        self.assertIn("Website site-test is down. Availability: 78.33%", alertingStream.getBuffer()[0])
        self.assertIn("Website site-test recovered", alertingStream.getBuffer()[1])

        # To verify that the recovery message in only showed once, we inscrease the availibity and check that no more
        # message is printed
        for i in range(0,48):
            checkWebsite(floatWindow_2min, 0.1, testWebsiteInput.checkInterval)


        self.assertEqual(len(alertingStream.getBuffer()), 2)
        self.assertIn("Website site-test is down. Availability: 78.33%", alertingStream.getBuffer()[0])
        self.assertIn("Website site-test recovered", alertingStream.getBuffer()[1])


    # In this scenario, we'll maintain the availability below 0.8 in the first 2 minuts. However, as 2 minuts haven't
    # already been passed, we expect that no alert will be showed. After two minuts, we expect an alert message
    def test_scenario_2(self):

        # Bufers where the Availability alert will print the alerting message
        globalStream = OutputBuffer()
        alertingStream = OutputBuffer()

        # We don't want to rely in a internet connection to test our class. Hence, we'll
        # create synthetic response each 2s and put it in the FloatWindow window
        # As the window has 2min length, a full window has 60 elements
        # We call an invalid element an element that represents that the server hasn't responded (-1)
        testWebsiteInput = WebsiteInput("site-test", 2000)
        floatWindow_2min = FloatWindow(2*60*1000, testWebsiteInput)
        availabilityAlert = AvailabilityAlert(floatWindow_2min, globalStream, alertingStream)

        #During the first 2min, we simulate an availabilty of 0.6

        # Insertion of valid elements
        for i in range(0,36):
            checkWebsite(floatWindow_2min, 0.1, testWebsiteInput.checkInterval)

        # Insertion of invalid elements (the server doesn't respond)
        for i in range(0,24):
            checkWebsite(floatWindow_2min, -1, testWebsiteInput.checkInterval)
            # As we didn't reach 2 minutes, no alert must be showed
            self.assertEqual(len(alertingStream.getBuffer()), 0)

        # Here 2 min has been passed. The next insertion should trigger an alert
        checkWebsite(floatWindow_2min, -1, testWebsiteInput.checkInterval)

        # Verify that only one alert has been showed
        self.assertEqual(len(alertingStream.getBuffer()), 1)
        self.assertIn("Website site-test is down. Availability: 58.33%", alertingStream.getBuffer()[0])


# Run the tests in this module
unittest.main()
