""" Module with a class that wraps the information about a website
"""

class WebsiteInput:
    """ This class wraps the information that the user must enter to ask to monitor a website
        That is: its name and a checkInterval (in miliseconds)
    """

    def __init__(self, name, checkInterval):
        self.name = name
        self.checkInterval = int(checkInterval)

    def __str__(self):
        return "name:{} checkInterval:{}".format(self.name, self.checkInterval)

    def __repr__(self):
        return "name:{} checkInterval:{}".format(self.name, self.checkInterval)
