""" This module contains a class to parse the config file
"""

from .website_input import WebsiteInput

class InputParser:
    """ Class to parse the config file (.cfg)
        Pass the filename as a parameter to the class constructor to create an object to parte the desired file
        Call the method parse to get a list of objects of the class WebsiteInputs with the website name and checkInterval time
    """

    # Contructor: gets the name of the file to be parsed
    def __init__(self, fileName):
        self.fileName = fileName

    def parse(self):
        """ Parse the file passed as a parameter to the constructor
            Returns a list of WebsiteInput objects containing the name and checkInterval of the sites listed on the config file
        """

        # Vector to store the WebsiteInput objects
        websiteInputs = []

        # Use with to not have to close the file at the end
        # A valid line is in the following format: {websiteName}|{checkInterval}
        with open(self.fileName) as input:

            for line in input:

                # If the line starts with a #, it's a commented line
                if line[0] == '#':
                    continue

                # If it's a valid line, split with | to get the website name and the check interval
                args = line.split('|')
                websiteInputs.append( WebsiteInput(args[0], args[1]) )

        return websiteInputs
