""" This module constains the class that handles the interface to show the alerting histric interface
"""

import npyscreen

class AlertingBuffer(npyscreen.FormBaseNew):
    """ Form to handle the interface to show the historic of triggered alerting messages
    """

    def create(self):
        """ Form initialization
        """

        # To call handle keyboard pressing every 1 ms
        self.keypress_timeout = 1

        # The form has an widget to show the streamBuffer
        self.stream = self.add(npyscreen.Pager )

        # If the user press control+B, the go_back method is called
        self.add_handlers({"^B": self.go_back})

    def go_back(self, key):
        """ Method called when the user press ctrl+B
            Goes back to the main menu form
        """
        self.parentApp.change_form("MAIN")

    def while_waiting(self):
        """ Method called to handle modifications on the form's widgets
        """

        # Get the strings in the alertingBuffer. The AvailabilityAlert objects could have printed
        # new data on this buffer
        self.stream.values = self.parentApp.alertingBuffer.getBuffer()

        # Refresh the widget
        self.stream.display()
