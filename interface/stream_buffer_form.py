""" This module constains the class that handles the interface to show the stream
    with the current stats and alerting messages
"""

import npyscreen

class StreamBuffer(npyscreen.FormBaseNew):
    """ Form to handle the interface to show the current stats and current alerting from the
        websites monitoring
    """

    def create(self):
        """ Form initialization
        """

        # To call handle keyboard pressing every 1 ms
        self.keypress_timeout = 1

        # The form has an widget to show the streamBuffer
        self.stream = self.add(npyscreen.Pager )

        # If the user press control+B, the go_back method is called
        self.add_handlers({"^B": self.go_back})

    def go_back(self, key):
        """ Method called when the user press ctrl+B
            Goes back to the main menu form
        """
        self.parentApp.change_form("MAIN")

    def beforeEditing(self):
        """ Before showing the form, clean the streamBuffer to only show current stats and alerting messages
        """

        # Clear the streamBuffer to show only the last stats
        self.parentApp.streamBuffer.clear()

    def while_waiting(self):
        """ Method called to handle modifications on the form's widgets
        """

        # Get the strings in the streamBuffer. The PrintStats objects or AvailabilityAlert objects could have printed
        # new data on this buffer
        self.stream.values = self.parentApp.streamBuffer.getBuffer()

        # We always show the last items in the stream
        self.stream.start_display_at = len(self.stream.values) - self.DEFAULT_LINES

        # Refresh the widget
        self.stream.display()
