""" This module constains the form to handle the dynamical input of a new website to monitor
"""

import npyscreen

from input import WebsiteInput
from monitoring import WebsiteMonitor

class InputSite(npyscreen.Form):
    """ Form to input dynamically a new website to be monitored
    """
    def create(self):
        """ Configuration of the form
        """

        # This form has an widget to store the new website name and another one to store the desired checkInterval
        self.siteName = self.add(npyscreen.TitleText, name='Website Address', use_two_lines=False, begin_entry_at=25)
        self.checkInterval = self.add(npyscreen.TitleText, name='Check Interval (in ms)', use_two_lines=False, begin_entry_at=25)

    def afterEditing(self):
        """ Method called after the user press the 'OK button'
        """

        # If the user has entered a new site to check with a corresponding checkInterval, create a corresponding websiteInput and a websiteMonitor
        if (self.siteName.value != "" and self.checkInterval.value != ""):
            newWebsiteInput = WebsiteInput(self.siteName.value, self.checkInterval.value)

            newWebsiteMonitor = WebsiteMonitor(newWebsiteInput, self.parentApp.streamBuffer, self.parentApp.alertingBuffer)
            newWebsiteMonitor.start()

            # Append them to the websiteInputs and websiteMonitors vector on the application class
            self.parentApp.websiteInputs.append(newWebsiteInput)
            self.parentApp.websiteMonitors.append(newWebsiteMonitor)

        # Reset the values for the next time that the form will be used
        self.siteName.value = None
        self.checkInterval.value = None

        # Return to the main menu
        self.parentApp.setNextForm("MAIN")
