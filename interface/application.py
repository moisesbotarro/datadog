""" This module contains the class that inherit from NSPAppManaged that manages
    the entire application and switch between the different application's form
"""

import npyscreen

from input import InputParser, WebsiteInput
from monitoring import WebsiteMonitor, OutputBuffer

from .main_menu_form import MainMenu
from .input_site_form import InputSite
from .stop_site_monitor_form import StopSiteMonitor
from .stream_buffer_form import StreamBuffer
from .alerting_buffer_form import AlertingBuffer

class MyApplication (npyscreen.NPSAppManaged):
    """ Class that manages the application. It holds all the application's form.
        To run an apllication, call the method run on an instane of this class
    """

    def __init__(self):
        npyscreen.NPSAppManaged.__init__(self)

        # The application has attributes that hold the necessary objects to monitor the websites
        self.websiteInputs = None # List of WebsiteInput objects that represent the sites to monitor
        self.alertingBuffer = None # Buffer to save the historic of alerting messages
        self.streamBuffer = None # Global buffer to print the stats and alerting messages. Each time the buffer is shown, it's cleared
        self.websiteMonitors = None # List of WebsiteMonitor objects. Each one represents the monitoring of a website


    def onStart(self):
        """ Initilization of the application
        """

        # Read the config files to start the monitoring of the websites that are inside of it
        inputParser = InputParser("config.cfg")
        self.websiteInputs = inputParser.parse()

        self.alertingBuffer = OutputBuffer()
        self.streamBuffer = OutputBuffer()
        self.websiteMonitors = []

        # Creating of a website monitor for each site on the config file
        for websiteInput in self.websiteInputs:
            self.websiteMonitors.append(WebsiteMonitor(websiteInput, self.streamBuffer, self.alertingBuffer))

        # Start the website monitors
        for websiteMonitor in self.websiteMonitors:
            websiteMonitor.start()

        # Application's Form
        self.addForm('MAIN', MainMenu, name='Main Menu') # Main Menu
        self.addForm('INPUT_SITE', InputSite, name='WebSite Input - Input the website\'s name and checlInterval. Press \'OK\' when finished') # Screen to enter a new website
        self.addForm('STOP_MONITORING', StopSiteMonitor, name='Select a website to stop the monitoring. Press \'OK\' to confirm and go back to the main menu') # Screen to stop a website monitoring
        self.addForm('GLOBAL_STREAM', StreamBuffer, name='Global Stats Stream, press ctrl+b to go back to the main menu') # Screen to show the current global stream with stats and alerting messages
        self.addForm('ALERTING_BUFFER', AlertingBuffer, name='Alerting Historic, press ctrl+b to go back to the main menu, use the arrows to navigate') # Screen to show alerting historic

    def change_form(self, name):
        """ This method changes to the form passed as an attribute
        """
        self.switchForm(name)
        self.resetHistory()
