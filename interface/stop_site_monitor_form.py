""" This module contains the Form class that handles the interface to stop the monitoring
    of a website
"""

import npyscreen

class StopSiteMonitor(npyscreen.Form):
    """ Form to handle the interface to stop a website monitoring
    """

    def create(self):
        """ Form's initialization
        """

        # Get the website's names to be shown as option
        self.websitesName = []
        for websiteMonitor in self.parentApp.websiteMonitors:
            self.websitesName.append(websiteMonitor.websiteInput.name)

        # Add an option that don't stop any of the monitored sites
        self.websitesName.append("I don't want to stop a webMonitoring")

        # Widget that display the options to stop the monitoring
        self.option = self.add(npyscreen.MultiLine,scroll_exit=True, values = self.websitesName)

        self.updated = False # Variable to allow us to update the Form if we go back to the main menu and then we enter the form again

    def beforeEditing(self):
        """ Method called before the Form is displayed
        """

        # We update the websites' name since the user could be entered a new website to be monitored
        if not self.updated:
            self.websitesName = []
            for websiteMonitor in self.parentApp.websiteMonitors:
                self.websitesName.append(websiteMonitor.websiteInput.name)

            self.websitesName.append("I don't want to stop a webMonitoring")
            self.option.values = self.websitesName

            # We do it only once when we'll show this form
            self.updated = True
            self.option.display()

    def afterEditing(self):
        """ Method called when the user press the 'OK' button
        """

        # If the user has chosen a website to stop its monitoring
        if self.option.value != None and self.option.value < len(self.parentApp.websiteMonitors):

            # Stop the websiteMonitoring
            self.parentApp.websiteMonitors[self.option.value].stop()

            # Delete this website monitoring from the WebsiteMonitos vector on the application
            del self.parentApp.websiteMonitors[self.option.value]

        # Reset the chosen value of the menu
        self.option.value = None
        self.updated = False

        # Change to the main menu Form 
        self.parentApp.setNextForm("MAIN")
