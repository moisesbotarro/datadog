""" This class contain the Main Menu Form to Display the principal options offered by the program
"""

import npyscreen

class MainMenu(npyscreen.FormBaseNew):

    def create(self):
        """ Configuration of the form
        """

        self.keypress_timeout = 1 # To call while_waiting every 1 ms

        # The Main Menu has a title, a menu title and a menu to choose the desired option
        self.title = self.add(npyscreen.FixedText, value="<<<<<<< Welcome to the Monitoring Application >>>>>>>")
        self.menuTitle = self.add(npyscreen.FixedText, rely=8, value="SELECT AN OPTION")
        self.option = self.add(npyscreen.MultiLine,scroll_exit=True, rely = 10, values = ['Input', 'Stop a Website Monitoring', 'Actual Global Stream', 'Alerting Historic', 'EXIT'])

    def while_waiting(self):
        """ This method reads the chosen item of the menu option and switch to the corresponding Form
        """

        # Go to Input Website Form
        if self.option.value == 0:
            self.option.value = None
            self.parentApp.change_form("INPUT_SITE")

        # Go to the Stop Monitoring Form
        if self.option.value == 1:
            self.option.value = None
            self.parentApp.change_form("STOP_MONITORING")

        # Go to the Global Stream Form to show the current stats and alerting
        if self.option.value == 2:
            self.option.value = None
            self.parentApp.change_form("GLOBAL_STREAM")

        # Go to the Alerting Historic Form
        if self.option.value == 3:
            self.option.value = None
            self.parentApp.change_form("ALERTING_BUFFER")

        # Quit the application
        if self.option.value == 4:
            self.option.value = None

            # Stop all websiteMonitors
            for websiteMonitor in self.parentApp.websiteMonitors:
                websiteMonitor.stop()

            self.parentApp.change_form(None)
