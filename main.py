""" This file launches the application
"""

from interface import MyApplication

if __name__ == "__main__":
    App = MyApplication()
    App.run()
    print ("Waiting all threads to shut down. This can be take up to 1 min. We don't like to kill threads :)")
    for websiteMonitor in App.websiteMonitors:
        websiteMonitor.waitThreadsToFinish()
    print("Finished")
